const http = require('http');

const app = require('./backend/app');

const server = http.createServer(app);

const onError = (err) => {
  console.log('here is ', err);
}

server.on('error', onError);
server.listen(3000);
