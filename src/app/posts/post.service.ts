import { Injectable } from '@angular/core';
import { Post } from './post.model';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

interface PostAPI {
  message: string;
  posts: Post[];
}

@Injectable({
  providedIn: 'root',
})
export class PostService {
  private posts: Post[] = [];
  private postUpdated = new Subject<Post[]>();

  constructor(private http: HttpClient, private router: Router) {}

  getPosts() {
    this.http
      .get('http://localhost:3000/api/posts')
      .pipe(
        map((postsData: any) => {
          return postsData.posts.map((post) => {
            return {
              title: post.title,
              content: post.content,
              id: post._id,
              imagePath: post.imagePath,
            };
          });
        })
      )
      .subscribe((postData: any) => {
        this.posts = postData;
        this.postUpdated.next(this.posts);
      });
  }

  getPost(postId: string): Observable<any> {
    return this.http.get('http://localhost:3000/api/post/' + postId);
  }
  addPost(title: string, content: string, image: File) {
    // tslint:disable-next-line: object-literal-shorthand
    // const post: Post = { id: null, title: title, content: content, imagePath: null};

    const postData = new FormData();
    postData.append('title', title);
    postData.append('content', content);
    postData.append('image', image, title);

    this.http
      .post('http://localhost:3000/api/posts', postData)
      .subscribe((res: any) => {
        console.log(res.message);
        const postAdded: Post = {
          id: res.id,
          title: res.title,
          content: res.content,
          imagePath: res.imagePath,
        };
        this.posts.push(postAdded);
        this.postUpdated.next(this.posts);
        this.router.navigate(['/']);
      });
  }

  deletePost(postID: string) {
    this.http
      .delete('http://localhost:3000/api/delete/' + postID)
      .subscribe((res) => {
        const updatedPosts = this.posts.filter((post) => post.id !== postID);
        this.posts = updatedPosts;
        this.postUpdated.next(this.posts);
      });
  }

  updatePost(id: string, title: string, content: string, image: File | string) {
    let postData: Post | FormData;

    if (typeof image === 'object') {
      postData = new FormData();
      postData.append('id', id);
      postData.append('title', title);
      postData.append('content', content);
      postData.append('image', image, title);
    } else {
      // tslint:disable-next-line: object-literal-shorthand
      postData = { id: id, title: title, content: content, imagePath: image };
    }

    this.http
      .put('http://localhost:3000/api/update/' + id, postData)
      .subscribe((response) => {
        // console.log(this.posts);
        this.router.navigate(['/']);
      });
  }

  getPostUpdateListner() {
    return this.postUpdated.asObservable();
  }
}
