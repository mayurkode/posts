import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Post } from '../post.model';
import { PostService } from '../post.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss'],
})
export class PostListComponent implements OnInit, OnDestroy {
  // posts: Post[] = new Array(
  //   { title: 'one', content: 'this is one ' },
  //   { title: 'two', content: 'this is two ' }
  // );

  posts: Post[] = [];
  isLoading = false;
  private postSubscribe = new Subscription();

  constructor(public postService: PostService) {}

  ngOnInit() {
    this.isLoading = true;
    this.postService.getPosts();
    this.postSubscribe = this.postService.getPostUpdateListner().subscribe(
      (posts: Post[]) => {
        this.isLoading = false;
        this.posts = posts;
      },
      (err) => {
        console.log(err);
      },
      () => {
        console.log('complete');
      }
    );
  }

  onDelete(postID: string) {
    this.postService.deletePost(postID)
  }

  ngOnDestroy() {
    this.postSubscribe.unsubscribe();
  }
}
