import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Post } from '../post.model';
import { PostService } from '../post.service';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.scss'],
})
export class PostCreateComponent implements OnInit {
  post: Post = { id: null, title: '', content: '', imagePath: null };
  isLoading = false;
  createPostForm: FormGroup;
  imagePreview: string;
  private mode = 'create';
  private postId: string;
  constructor(public postService: PostService, public route: ActivatedRoute) {}

  ngOnInit(): void {
    this.createPostForm = new FormGroup({
      title: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(2)],
      }),
      content: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(2)],
      }),
      image: new FormControl(null, { validators: [Validators.required] }),
    });

    this.route.paramMap.subscribe((params) => {
      if (params.has('postId')) {
        this.mode = 'edit';
        this.postId = params.get('postId');
        this.isLoading = true;
        this.postService.getPost(this.postId).subscribe((postData: any) => {
          this.isLoading = false;
          this.post.id = postData._id;
          this.post.title = postData.title;
          this.post.content = postData.content;
          this.post.imagePath = postData.imagePath;
          // this.createPostForm.setValue({
          //   title: postData.title,
          //   content: postData.content,
          // });
          this.createPostForm.patchValue({
            title: postData.title,
            content: postData.content,
            imagePath: postData.imagePath,
          });
          this.createPostForm.get('title').updateValueAndValidity();
          this.createPostForm.get('content').updateValueAndValidity();
          this.createPostForm.get('image').updateValueAndValidity();
        });
        console.log(this.post);
      } else {
        this.mode = 'create';
        this.postId = null;
        console.log(this.post);
      }
    });
  }

  onImagePicked(event) {
    const file = event.target.files[0];
    console.log(file);
    this.createPostForm.patchValue({ image: file });
    this.createPostForm.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

  resetImage() {
    this.imagePreview = null;
    this.createPostForm.patchValue({ image: null });
    this.createPostForm.get('image').updateValueAndValidity();
  }
  onSavePost() {
    if (this.createPostForm.invalid) {
      return;
    }

    this.isLoading = true;
    if (this.mode === 'create') {
      this.postService.addPost(
        this.createPostForm.value.title,
        this.createPostForm.value.content,
        this.createPostForm.value.image
      );
    } else if (this.mode === 'edit') {
      console.log('edit', this.postId);
      this.postService.updatePost(
        this.postId,
        this.createPostForm.value.title,
        this.createPostForm.value.content,
        this.createPostForm.value.image
      );
    }
    this.createPostForm.reset();
  }
}
