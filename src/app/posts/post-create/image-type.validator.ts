import { of as observableOf, Observable } from 'rxjs';
import {
  NG_ASYNC_VALIDATORS,
  AbstractControl,
  ValidationErrors,
  AsyncValidator,
} from '@angular/forms';
import { Directive } from '@angular/core';

@Directive({
  selector: '[appImageTypeValidator]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: ImageTypeValidatorDirective,
      multi: true,
    },
  ],
})
export class ImageTypeValidatorDirective implements AsyncValidator {
  validate(control: AbstractControl): Observable<ValidationErrors | null> {

    console.log(control.value);
    if (control) {
      return null;
    }
    return observableOf({ custom: true });
  }
}
