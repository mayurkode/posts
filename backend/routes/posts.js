const express = require('express');
const multer = require('multer');
const router = express.Router();

const Post = require('../models/post');

const MIME_TYPE_MAP = {
  'image/gif': 'gif',
  'image/png': 'png',
  'image/svg+xml': 'svg',
  'image/jpeg': 'jpg',
  'image/jpg': 'jpg'
}

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    const isValid = MIME_TYPE_MAP[file.mimetype];
    let error = new Error('invalid mime type');
    if (isValid) {
      error = null;
    }
    callback(null, 'backend/images');
  },
  filename: (req, file, callback) => {
    const filename = file.originalname.toLowerCase().split(' ').join('-');
    const ext = MIME_TYPE_MAP[file.mimetype];
    callback(null, filename + '-' + Date.now() + '.' + ext);
  }
});


// save post on db
router.post('/api/posts',
  multer({
    storage: storage
  }).single('image'),
  (req, res, next) => {

    const url = req.protocol + '://' + req.get('host');

    const post = new Post({
      title: req.body.title,
      content: req.body.content,
      imagePath: url + "/images/" + req.file.filename
    });

    console.log('going to add this post: ', post);

    post.save().then(result => {
      res.status(201).json({
        message: 'New post addded successfully',
        id: result._id,
        title: result.title,
        content: result.content,
        imagePath: result.imagePath
      });
    }); // save to database


  });

// This is to update one post
router.put('/api/update/:id', multer({
  storage: storage
}).single('image'), (req, res) => {

  console.log('', req.file);
  let imagePath = req.body.imagePath;

  if (req.file) {
    const url = req.protocol + '://' + req.get('host');
    imagePath = url + "/images/" + req.file.filename;

    console.log('imagePath ', imagePath);
  }

  const post = new Post({
    _id: req.params.id,
    title: req.body.title,
    content: req.body.content,
    imagePath: imagePath
  });

  Post.updateOne({
      _id: req.params.id
    }, post)
    .then(result => {
      res.status(200).json({
        message: 'Post is updated'
      });
    });
});


// This is to get all posts
router.use('/api/posts', (req, res, next) => {
  const posts = [{
      id: 1,
      title: 'Spiderman',
      content: 'he fires webs'
    },
    {
      id: 2,
      title: 'Ironman',
      content: 'He flies with fire'
    }
  ];

  Post.find().then(documents => {
    // console.log(documents);
    res.status(200).json({
      msg: 'posts fetched!!',
      posts: documents
    });

  });

});

// To get one post
router.use('/api/post/:id', (req, res) => {

  Post.findById(req.params.id).then(post => {
    res.status(200).json(post);
  });
});

// Delete post with post ID
router.delete('/api/delete/:id', function (req, res) {


  Post.deleteOne({
      _id: req.params.id
    })
    .then(() => {
      res.status(200).json({
        message: req.param.id + 'post deleted'
      });

    });

});


module.exports = router;
