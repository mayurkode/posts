const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const postsRoutes = require('./routes/posts');
const app = express();
const mongoose = require('mongoose');

// connection to DB
mongoose.connect('mongodb+srv://max:hjFjlmAvHawFo667@cluster0-cmsnh.mongodb.net/test?retryWrites=true&w=majority')
  .then(() => {
    console.log('connected to Database at MongoDB');
  })
  .catch(() => {
    console.log('connection failed');
  });

// max :  hjFjlmAvHawFo667   --- username and password for db

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use("/images", express.static("backend/images"));

// setting CORS
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

app.use(postsRoutes);

module.exports = app;
